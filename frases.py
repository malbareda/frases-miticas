
import discord
from discord.ext import commands
from discord.ext.commands import Bot
import datetime
import hashlib
import sqlite3

#prefix
bot = commands.Bot(command_prefix='!')

#check if database is made and load it
db = sqlite3.connect('quotes.db')
cursor = db.cursor()
cursor.execute('CREATE TABLE IF NOT EXISTS quotes(hash TEXT primary key, user TEXT, message TEXT, date_added TEXT)')
print("BD Cargada")

db.commit()

@bot.event
async def on_ready():
    print ("Conectado a Discord")

#######commands##########

#test commmand
@bot.command(pass_context=True)
async def ping(ctx):
    await ctx.send("pong")
    print ("ping")

#UFFF
@bot.command(pass_context=True)
@commands.has_permissions(administrator=True)
async def borrar(ctx, *, message: str):
    string = str(message)
    print (string)
    cursor.execute("DELETE FROM quotes WHERE message =(?)",(string,))
    await ctx.send("Frase borrada con Ã©xito")


#help menu
@bot.command(pass_context=True)
async def ayuda(ctx):
    embed = discord.Embed(name="Ayuda")
    embed.set_author(name="Frases MÃ­ticas:")
    embed.add_field(name="Para aÃ±adir frase:", value="!frases @[usuario de discord] [frase completa]", inline=False)
    embed.add_field(name="Ejemplo:", value="!frase @wola No soy nazi", inline=False)
    embed.add_field(name="Para mostrar una frase random de un usuario", value="!frasede @[usuario]", inline=False)
    embed.add_field(name="Para mostrar todas las frases de un usuario", value="!frasesde @[usuario]", inline=False)
    embed.add_field(name="Para mostrar una frase random", value="!fraserandom", inline=False)
    await ctx.send(embed=embed)

#print random quote
@bot.command(pass_context=True)
async def fraserandom(ctx):

    cursor.execute("SELECT user,message,date_added FROM quotes ORDER BY RANDOM() LIMIT 1")
    query = cursor.fetchone()

    #log
    print(query[0]+": \""+query[1]+"\" escrita en pantalla "+str(query[2]))

    #embeds the output
    style = discord.Embed(name="frase", description="- "+str(query[0])+" "+str(query[2]))
    style.set_author(name=str(query[1]))
    await ctx.send(embed=style)


@bot.command(pass_context=True)
async def frases(ctx, *, message: str):

    #split the message into words
    string = str(message).strip()
    temp = string.split()

    if len(temp)<2:
        await ctx.send("Treedi eres retrasado")
        return

    print(string)
    print(temp)
    #take the username out
    user = temp[0]
    del temp[0]

    #join the message back together
    text = " ".join(temp)
    
    if user[1]!='@':
        print("sin arroba")

        
    



    uniqueID = hash(user+message)

    #date and time of the message
    time = datetime.datetime.now()
    formatted_time = str(time.strftime("%d-%m-%Y %H:%M"))

    #find if message is in the db already
    cursor.execute("SELECT count(*) FROM quotes WHERE hash = ?",(uniqueID,))
    find = cursor.fetchone()[0]

    if find>0:
        return

    #insert into database
    cursor.execute("INSERT INTO quotes VALUES(?,?,?,?)",(uniqueID,user,text,formatted_time))
    await ctx.send("Frase aÃ±adida con Ã©xito")

    db.commit()

    #number of words in the database
    rows = cursor.execute("SELECT * from quotes")

    #log to terminal
    print(str(len(rows.fetchall()))+". added - "+str(user)+": \""+str(text)+"\" to database at "+formatted_time)


@bot.command(pass_context=True)
async def frasede(ctx, message: str):
    
    #sanitise name
    user = (message,)

    try:
        #query random quote from user
        cursor.execute("SELECT message,date_added FROM quotes WHERE user=(?) ORDER BY RANDOM() LIMIT 1",user)
        query = cursor.fetchone()

        #adds quotes to message
        output = "\""+str(query[0])+"\""

        #log
        print(message+": \""+output+"\" printed to the screen "+str(query[1]))

        #embeds the output to make it pretty
        style = discord.Embed(name="frase", description="- "+message+" "+str(query[1]))
        style.set_author(name=output)
        await ctx.send(embed=style)

    except Exception:

        await ctx.send("Este usuario es un pleb aburrido")

    db.commit()    

@bot.command(pass_context=True)
async def frasesde(ctx, message: str):
    
    #sanitise name
    user = (message,)

    try:
        #query random quote from user
        cursor.execute("SELECT message,date_added FROM quotes WHERE user=(?)",user)
        query = cursor.fetchall()

        #adds quotes to message
        for q in query:
            output = "\""+str(q[0])+"\""

            #log
            print(message+": \""+output+"\" printed to the screen "+str(q[1]))

            #embeds the output to make it pretty
            style = discord.Embed(name="frase", description="- "+message+" "+str(q[1]))
            style.set_author(name=output)
            await ctx.send(embed=style)

    except Exception:

        await ctx.send("Este usuario es un pleb aburrido")

    db.commit()   

bot.run("token del bot aquí, gràcies.")
